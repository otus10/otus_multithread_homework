﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_multithread_homework
{
    public class Timer : ITimer
    {
        public TimeSpan CalculateTime(int[] array, Func<int[], long> func, out long sum)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            sum = func.Invoke(array);
            startTime.Stop();
            return startTime.Elapsed;
        }
    }
}
