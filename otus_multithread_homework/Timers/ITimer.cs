﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_multithread_homework
{
    public interface ITimer
    {
        public TimeSpan CalculateTime(int[] array, Func<int[], long> func, out long sum);
    }
}
