﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_multithread_homework
{
    public class ArrayManager
    {
        public int[] InitializeArray(int arraySize)
        {
            int[] array = new int[arraySize];
            for (int i = 0; i < arraySize; i++)
            {
                array[i] = 1;
            }
            return array;
        }
    }
}
