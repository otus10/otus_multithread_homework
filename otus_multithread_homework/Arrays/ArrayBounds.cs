﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_multithread_homework
{
    public class ArrayBounds
    {
        public int LowerBound { get; set; }
        public int UpperBound { get; set; }

        public ArrayBounds(int lowerBound, int upperBound)
        {
            LowerBound = lowerBound;
            UpperBound = upperBound;
        }
 
    }
}
