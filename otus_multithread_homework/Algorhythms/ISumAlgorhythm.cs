﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_multithread_homework
{
    public interface ISumAlgorhythm
    {
        public string Name { get;}
        public long SumInt(int[] array);
    }
}
