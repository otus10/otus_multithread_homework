﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_multithread_homework
{
    public class SumGeneral : ISumAlgorhythm
    {
        public string Name => "General";

        public long SumInt(int[] array)
        {
            long result = 0;
            for (int i = 0; i < array.Length; i++)
            {
                result += array[i];
            }
            return result; 
        }
    }
}
