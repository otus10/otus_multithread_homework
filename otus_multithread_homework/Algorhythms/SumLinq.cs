﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_multithread_homework
{
    public class SumLinq : ISumAlgorhythm
    {
        public string Name => "Linq";
        public long SumInt(int[] array)
        {
            return array.AsParallel().Sum(s => s);
        }
    }
}
