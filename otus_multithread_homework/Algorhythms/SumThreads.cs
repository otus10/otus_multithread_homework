﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_multithread_homework
{
    public class SumThreads : ISumAlgorhythm
    {
        private int _threads;
        public string Name => $"Threads {_threads}";

        public SumThreads(int threads)
        {
            _threads = threads;
        }
        public long SumInt(int[] array)
        {
            List<Task<long>> taskList = new List<Task<long>>();
            foreach (ArrayBounds arrayBounds in SplitArray(array, _threads))
            {
                taskList.Add(new Task<long>(() =>
                                {
                                    return SumArrayPart(arrayBounds.LowerBound, arrayBounds.UpperBound, array);
                                })
                            );

            }
            taskList.ForEach(task => task.Start());
            Task.WaitAll(taskList.ToArray<Task<long>>());
            return taskList.Sum(s => s.Result);
        }

        private long SumArrayPart(int startPosition, int endPosition, int[] array)
        {
            long result = 0;
            for (int i = startPosition; i <= endPosition; i++)
            {
                result += array[i];
            }
            return result;
        }

        private IEnumerable<ArrayBounds> SplitArray(int[] array, int parts)
        {
            int partSize = (int)Math.Ceiling(array.Length / Convert.ToDecimal(parts));
            ArrayBounds currentBounds = new ArrayBounds(0, TrimArrayUpperBound(partSize, array));
            bool firstPart = true;
            do
            {
                if (firstPart)
                {
                    firstPart = false;
                }
                else
                {
                    int upperBound = TrimArrayUpperBound(currentBounds.UpperBound + partSize, array);
                    currentBounds = new ArrayBounds(currentBounds.UpperBound + 1, upperBound);
                }
                yield return currentBounds;
            }
            while (currentBounds.UpperBound < array.Length - 1);


        }
        private int TrimArrayUpperBound(int upperBound, int[] array)
        {
            return upperBound < array.Length - 1
                                ? upperBound
                                : array.Length - 1;
        }
    }
}
