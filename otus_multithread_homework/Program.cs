﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace otus_multithread_homework
{
    class Program
    {
        static async Task Main(string[] args)
        {
            ITimer timer = new Timer();
            ISumAlgorhythm sumGeneral = new SumGeneral(); 
            ISumAlgorhythm sumLinq = new SumLinq();
            ISumAlgorhythm sumThreads6 = new SumThreads(6);
            Benchmark benchmark = new Benchmark(timer)
                                .AddAlgorhytm(sumGeneral)
                                .AddAlgorhytm(sumLinq)
                                .AddAlgorhytm(sumThreads6)
                                .AddBenchmark(100000)
                                .AddBenchmark(1000000)
                                .AddBenchmark(10000000)
                                ;
            await benchmark.Run();
            Console.ReadKey();
        }
    }
}
