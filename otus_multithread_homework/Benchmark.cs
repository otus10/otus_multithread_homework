﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_multithread_homework
{
    public class Benchmark
    {
        private readonly IList<ISumAlgorhythm> _algorhythms = new List<ISumAlgorhythm>();
        private readonly IList<int> _elementsBenchmark = new List<int>();
        private readonly ITimer _sumTimer;
        private readonly ArrayManager _arrayManager = new ArrayManager();
        public Benchmark(ITimer sumTimer)
        {
            _sumTimer = sumTimer;
        }

        public async Task Run()
        {
            await WarmUp();
            foreach (int elements in _elementsBenchmark)
            {
                int[] benchArray = _arrayManager.InitializeArray(elements);
                foreach (ISumAlgorhythm algorhythm in _algorhythms)
                {
                    string result = await executeBenchmark(benchArray, algorhythm);
                    Console.WriteLine(result);
                }
            }
        }

        private async Task<string> executeBenchmark(int[] benchArray, ISumAlgorhythm algorhythm)
        {
            string result = await Task.Run(() =>
             {
                 TimeSpan timeResult = new TimeSpan();
                 timeResult = _sumTimer.CalculateTime(benchArray, algorhythm.SumInt, out long sum);
                 return $"Array elements number: {benchArray.Length}. Alghorhytm: {algorhythm.Name}. Elapsed time {timeResult.TotalMilliseconds}. Sum: {sum} ";
             });
            return result;  
        }

        public Benchmark AddAlgorhytm(ISumAlgorhythm sumAlgorhythm)
        {
            _algorhythms.Add(sumAlgorhythm);
            return this;
        }

        public Benchmark AddBenchmark(int elements)
        {
            _elementsBenchmark.Add(elements);
            return this;
        }

        private async Task WarmUp()
        {
            foreach (ISumAlgorhythm algorhythm in _algorhythms)
            {
                int[] benchArray = _arrayManager.InitializeArray(1);
                await executeBenchmark(benchArray, algorhythm);
            }
        }
    }
}
